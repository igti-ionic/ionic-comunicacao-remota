/**
 * Fonte de inspiração:
 * https://www.djamware.com/post/59fc9da680aca7739224ee20/ionic-3-and-angular-5-mobile-app-example
 */

import { Component } from '@angular/core';
import { Http } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  /**
   * URL que é a fonte de dados dos países
   */
  private apiUrl = 'https://restcountries.eu/rest/v2/all';

  /**
   * Vetor de países, a ser preenchido
   */
  public paises: any;

  constructor(private http: Http) {}

  /**
   * A obtenção de países é feita
   * após o carregamento da tela
   */
  ionViewDidLoad() {
    this.obterPaises();
  }

  /**
   * Requisição assíncrona com promise
   */
  obterPaises() {
    this.http
      .get(this.apiUrl)
      .map(resposta => resposta.json())
      .toPromise()
      .then(dados => (this.paises = dados));
  }
}
